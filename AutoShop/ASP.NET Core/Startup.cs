﻿using ASP.NET_Core.Data.interfaces;
using ASP.NET_Core.Data.mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using ASP.NET_Core.Data;
using Microsoft.EntityFrameworkCore;
using ASP.NET_Core.Data.Repository;
using ASP.NET_Core.Data.Models;

namespace ASP.NET_Core
{
    public class Startup
    {
        private IConfigurationRoot _confString;
        public Startup(IHostingEnvironment host)
        {
            _confString = new ConfigurationBuilder().SetBasePath(host.ContentRootPath).AddJsonFile("dbsettings.json").Build();

        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AddDBContent>(options => options.UseSqlServer(_confString.GetConnectionString("DefaultConnection")));  
            services.AddTransient<iCarsCategorys, CategoryRespository>();
            services.AddTransient<IAllCars, CarRepository>();
            services.AddTransient<IAllOrders, OrdersRepository>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped(sp => ShopCart.GetCart(sp));
            services.AddMvc();
            services.AddMemoryCache();
            services.AddSession();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            app.UseDeveloperExceptionPage();
            app.UseStatusCodePages();
            app.UseStaticFiles();
            app.UseSession();
            // app.UseMvcWithDefaultRoute();
            app.UseMvc(routes => {
                routes.MapRoute(name: "default", template: "{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute(name: "categoryFilter", template: "Cars/{action}/{category?}", defaults: new { Controller = "Cars", action = "List" });
            });

            using (var scope = app.ApplicationServices.CreateScope())
            {
                AddDBContent content = scope.ServiceProvider.GetRequiredService<AddDBContent>();
                DBObjects.Initial(content);
            }
            
        }
    }
}
