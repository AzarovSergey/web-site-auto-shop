﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NET_Core.Data.Models
{
    public class ShopCart
{
        private readonly AddDBContent appdbcontetnt;
        public ShopCart(AddDBContent appdbcontetn)
        {
            this.appdbcontetnt = appdbcontetn; //&&
        }
        public string ShopCartId { get; set; }
        public List<ShopCartItem> listshopitems { get; set; }

        public static ShopCart GetCart(IServiceProvider services)
        {
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?.HttpContext.Session;
            var context = services.GetService<AddDBContent>();
            string shopCarId = session.GetString("CartId") ?? Guid.NewGuid().ToString();
            session.SetString("CartId", shopCarId);

            return new ShopCart(context) { ShopCartId = shopCarId };
        }
        public void  AddToCart(Car car)
        {
            appdbcontetnt.ShopCartItem.Add(new ShopCartItem
            {
                shopCartId=ShopCartId,
                car = car,
                price = car.price
            });

            appdbcontetnt.SaveChanges();
        }

        public List<ShopCartItem> getShopItems()
        {
            return appdbcontetnt.ShopCartItem.Where(c => c.shopCartId == ShopCartId).Include(s => s.car).ToList();
        }
}
}
