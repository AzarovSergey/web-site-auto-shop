﻿using ASP.NET_Core.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NET_Core.Data.interfaces
{
    public interface IAllOrders
    {
    void CreateOrder(Order order);
    }
}
