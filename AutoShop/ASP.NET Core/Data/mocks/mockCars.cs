﻿using ASP.NET_Core.Data.Models;
using ASP.NET_Core.Data.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NET_Core.Data.mocks
{
    public class mockCars : IAllCars
{

    private readonly iCarsCategorys _CategoryCars = new mockCategory();
    public IEnumerable<Car> Cars
    {
        get {
            return new List<Car> {
            new Car {
                name = "ВАЗ 2107",
                shortDesc ="Автомобили с ДВС",
                longDesc ="Автомобиль разработан в СССР",
                img = "/img/s1200.jpg",
                price = 4500,
                isFavourite = true,
                available =true,
                Category = _CategoryCars.AllCategorys.Last() },
            new Car {
                name = "Tesla",
                shortDesc ="Электромобиль",
                longDesc ="Разработал Илон Маск",
                img = "/img/s1201.jpg",
                price = 40500,
                isFavourite = true,
                available =true,
                Category = _CategoryCars.AllCategorys.First() },
            };
        }
    }
    public IEnumerable<Car> getFavCars { get ; set ; }

    public Car getObjectCar(int carId)
    {
        throw new NotImplementedException();
    }
}
}
