﻿using ASP.NET_Core.Data.Models;
using ASP.NET_Core.Data.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NET_Core.Data.mocks
{
    public class mockCategory : iCarsCategorys
    {
        public IEnumerable<Category> AllCategorys
        {
            get
            {
                return new List<Category>
        {
            new Category {categoryName="Электромобили",desc = "Автомобилий работающий на электричестве"},
            new Category {categoryName="Автомобили с ДВС",desc = "Автомобили работающий на бензине"}
        };
            }
        }

    }
}
