﻿using ASP.NET_Core.Data.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NET_Core.Data
{
    public class DBObjects
{
        public static void Initial(AddDBContent content)
        {
           
                
          /* if(!content.Category.Any())
            {
                content.Category.AddRange(Categories.Select(c=> c.Value));
            }*/
            if (!content.Car.Any())
            {
                content.AddRange(
                                new Car
                                {
                                    name = "ВАЗ 2107",
                                    shortDesc = "Автомобили с ДВС",
                                    longDesc = "Автомобиль разработан в СССР",
                                    img = "/img/s1200.jpg",
                                    price = 4500,
                                    isFavourite = true,
                                    available = true,
                                    Category = Categories["Автомобили с ДВС"]
                                },
                                            new Car
                                            {
                                                name = "Tesla",
                                                shortDesc = "Электромобиль",
                                                longDesc = "Разработал Илон Маск",
                                                img = "/img/s1201.jpg",
                                                price = 40500,
                                                isFavourite = true,
                                                available = true,
                                                Category = Categories["Электромобили"]
                                            }
                
            );
                    
            }
            content.SaveChanges();
        }

        private static Dictionary<string, Category> category;
        public static Dictionary<string, Category> Categories
        {
            get
            {
                if (category == null)
                {
                    var list = new Category[]
                    {
                        new Category {categoryName="Электромобили",desc = "Автомобилий работающий на электричестве"},
                        new Category {categoryName="Автомобили с ДВС",desc = "Автомобили работающий на бензине"}
                    };

                    category = new Dictionary<string, Category>();
                    foreach(Category el in list)
                    {
                        category.Add(el.categoryName, el);
                    }
                }
                return category;
            }
        }
}
}
