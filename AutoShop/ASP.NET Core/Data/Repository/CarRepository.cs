﻿using ASP.NET_Core.Data.interfaces;
using ASP.NET_Core.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NET_Core.Data.Repository
{
    public class CarRepository : IAllCars
    {

        private readonly AddDBContent appdbcontetnt;
        public CarRepository(AddDBContent appdbcontetn)
        {
            this.appdbcontetnt = appdbcontetn; //&&
        }
        public IEnumerable<Car> Cars => appdbcontetnt.Car.Include(c=> c.Category);

        public IEnumerable<Car> getFavCars => appdbcontetnt.Car.Where(p => p.isFavourite).Include(c => c.Category);

        public Car getObjectCar(int carId) => appdbcontetnt.Car.FirstOrDefault(p => p.id == carId);
        
    }
}
