﻿using ASP.NET_Core.Data.interfaces;
using ASP.NET_Core.Data.Models;
using ASP.NET_Core.Data.Repository;
using ASP.NET_Core.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NET_Core.Controlers
{
    public class ShopCartController : Controller
{
        private readonly IAllCars _carRep;
        private readonly ShopCart _shopCart;

        public ShopCartController(IAllCars carRep, ShopCart shopCart)
        {
            _carRep = carRep;
            _shopCart = shopCart;
        }

        public ViewResult Index()
        {
            ViewBag.Title = "Корзина-AutoShop";
            var items = _shopCart.getShopItems();
            _shopCart.listshopitems = items;

            var obj = new ShopCartViewModelcs
            {
                shopCart = _shopCart
            };

        return View(obj);
        }

        public RedirectToActionResult addToCart(int id)
        {
            var item = _carRep.Cars.FirstOrDefault(i => i.id == id);
            if(item != null)
            {
                _shopCart.AddToCart(item);
            }
            return RedirectToAction("Index");
        }
}
}
