﻿using ASP.NET_Core.Data.interfaces;
using ASP.NET_Core.Data.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NET_Core.Controlers
{
    public class OrderController : Controller
    {
        private readonly IAllOrders allorders;
        private readonly ShopCart shopCart;

        public OrderController(IAllOrders allorders, ShopCart shopCart)
        {
            this.allorders = allorders;
            this.shopCart = shopCart;
        }

        public IActionResult Checkout()
        {
            ViewBag.Title = "Заказ-AutoShop";
            return View();
        }

        [HttpPost]
        public IActionResult Checkout(Order order)
        {
            ViewBag.Title = "Заказ-AutoShop";
            shopCart.listshopitems = shopCart.getShopItems();
            if(shopCart.listshopitems.Count==0)
            {
                ModelState.AddModelError("","У вас должны быть товары");
            }

            if(ModelState.IsValid)
            {
                allorders.CreateOrder(order);
                return RedirectToAction("Complete");
            }
            return View(order);
        }

        public IActionResult Complete()
        {
            ViewBag.Title = "Заказ-AutoShop";
            ViewBag.Message = "Заказ успешно обработан";
            return View();
        }
    }
}
