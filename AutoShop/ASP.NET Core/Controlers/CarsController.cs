﻿using ASP.NET_Core.Data.interfaces;
using ASP.NET_Core.Data.Models;
using ASP.NET_Core.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NET_Core.Controlers
{
    public class CarsController : Controller
{
        private readonly IAllCars _allcars;
        private readonly iCarsCategorys _allcategory;

        public CarsController(IAllCars iallcars, iCarsCategorys icarscat)
        {
            _allcars = iallcars;
            _allcategory = icarscat;
        }
        [Route("Cars/List")]
        [Route("Cars/List/{category}")]
        public ViewResult List(string category)
        {
            ViewBag.Title = "Каталог-AutoShop";
            string _category = category;
            IEnumerable<Car> cars=null;
            string currCategory = "";
            if(string.IsNullOrEmpty(category))
            {
                cars = _allcars.Cars.OrderBy(i => i.id);
            }
            else
            {
                if(string.Equals("electro", category,StringComparison.OrdinalIgnoreCase ))
                {
                    cars = _allcars.Cars.Where(i => i.Category.categoryName.Equals("Электромобили")).OrderBy(i => i.id);
                    currCategory = "Электромобили";
                }
                else if(string.Equals("full", category, StringComparison.OrdinalIgnoreCase))
                {
                    cars = _allcars.Cars.Where(i => i.Category.categoryName.Equals("Автомобили с ДВС")).OrderBy(i => i.id);
                    currCategory = "Автомобили с ДВС"; 
                }

                
          
            }

            var carObj = new CarsListViewModel
            {
                
                getallcars = cars,
                carCategory = currCategory // ??
            };
            
       


           

            return View(carObj);
        }


}
}
