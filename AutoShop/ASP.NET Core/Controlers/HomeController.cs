﻿using ASP.NET_Core.Data.interfaces;
using ASP.NET_Core.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NET_Core.Controlers
{
    public class HomeController : Controller
{
        private readonly IAllCars _carRep;
        

        public HomeController(IAllCars carRep)
        {
            _carRep = carRep;
            
        }

        public ViewResult Index()
        {
            ViewBag.Title = "Главная-AutoShop";
            var homeCars = new HomeViewModel
            {
              favCars= _carRep.getFavCars
            };
            
               
            return View(homeCars);
        }
    }
}
